﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameObject))]
public class Player : MonoBehaviour
{
    [SerializeField] private GameObject falseCartesianGO;
    [SerializeField] private float speed = 20.0f;

    private FalseCartesian falseCartesian;

    private void Start()
    {
        falseCartesian = falseCartesianGO.GetComponent<FalseCartesian>();
    }

    public void moveLeft()
    {
        Vector3 force = (-1.0f * speed * Time.deltaTime) * falseCartesian.FalseX;
        gameObject.GetComponent<Rigidbody>().AddForce(force, ForceMode.Force);
    }

    public void moveRight()
    {
        Vector3 force = (1.0f * speed * Time.deltaTime) * falseCartesian.FalseX;
        gameObject.GetComponent<Rigidbody>().AddForce(force, ForceMode.Force);
    }

    public void moveUp()
    {
        Vector3 force = (1.0f * speed * Time.deltaTime) * falseCartesian.FalseZ;
        gameObject.GetComponent<Rigidbody>().AddForce(force, ForceMode.Force);
    }

    public void moveDown()
    {
        Vector3 force = (-1.0f * speed * Time.deltaTime) * falseCartesian.FalseZ;
        gameObject.GetComponent<Rigidbody>().AddForce(force, ForceMode.Force);
    }
}
