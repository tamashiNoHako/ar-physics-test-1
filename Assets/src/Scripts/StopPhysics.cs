﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class StopPhysics : MonoBehaviour
{

    public ImageTargetBehaviour myTarget;
    public Rigidbody myRigidBody;

    private void Start()
    {
        Debug.Log(Physics.gravity);

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //Debug.Log ("**********************************************");
        //Debug.Log ("Image Target Status: " + myTarget.CurrentStatus);
        Debug.Log(myRigidBody.transform.position);
        if (myTarget.CurrentStatus == TrackableBehaviour.Status.NOT_FOUND)
        {
            myRigidBody.Sleep();
        }
        else
        {
            if (myRigidBody.IsSleeping())
            {
                myRigidBody.WakeUp();
            }
        }

        // NOT_FOUND
        // TRACKED

    }
}