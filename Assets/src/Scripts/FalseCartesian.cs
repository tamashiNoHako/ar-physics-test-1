﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameObject))]
public class FalseCartesian : MonoBehaviour
{

    [SerializeField] private GameObject pointO, pointX, pointZ;
    private Vector3 vecO, vecX, vecZ, vecOX, vecOZ;

    public Vector3 FalseX { get { return vecOX.normalized; } }
    public Vector3 FalseZ { get { return vecOZ.normalized; } }

    private void FixedUpdate()
    {
        vecO = pointO.transform.position;
        vecX = pointX.transform.position;
        vecZ = pointZ.transform.position;
        vecOX = vecX - vecO;
        vecOZ = vecZ - vecO;
        //Debug.Log("Vector OX\t" + Vector3.Magnitude(vecOX).ToString());
        //Debug.Log("Vector OZ\t" + Vector3.Magnitude(vecOZ).ToString());
    }

}
