﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameObject))]
public class GravityModifier : MonoBehaviour
{
    [SerializeField] private GameObject pointP, pointQ, pointR;

    private Vector3 vectorP, vectorQ, vectorR;

    private void FixedUpdate()
    {
        //Debug.Log(pointP.transform.localPosition);
        vectorP = pointP.transform.position;
        vectorQ = pointQ.transform.position;
        vectorR = pointR.transform.position;
        Vector3 vectorPQ = vectorQ - vectorP;
        Vector3 vectorPR = vectorR - vectorP;
        Vector3 normalizedNormal = Vector3.Cross(vectorPQ, vectorPR).normalized;
        float gravityModulus = 9.8f;
        Physics.gravity = gravityModulus * (-1 * normalizedNormal);
    }

}
